﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FilmSitesi.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FilmSitesi.Resources.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Abone Ol.
        /// </summary>
        public static string AboneOl {
            get {
                return ResourceManager.GetString("AboneOl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anasayfa.
        /// </summary>
        public static string Anasayfa {
            get {
                return ResourceManager.GetString("Anasayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Arama.
        /// </summary>
        public static string Arama {
            get {
                return ResourceManager.GetString("Arama", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to destek.
        /// </summary>
        public static string destek {
            get {
                return ResourceManager.GetString("destek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En iyi Filmler.
        /// </summary>
        public static string EniyiFilmler {
            get {
                return ResourceManager.GetString("EniyiFilmler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Film Bilgi Sistemi.
        /// </summary>
        public static string FilmBilgiSistemi {
            get {
                return ResourceManager.GetString("FilmBilgiSistemi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Güncel Filmler.
        /// </summary>
        public static string GüncelFilm {
            get {
                return ResourceManager.GetString("GüncelFilm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımızda.
        /// </summary>
        public static string Hakkimizda {
            get {
                return ResourceManager.GetString("Hakkimizda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hedefler.
        /// </summary>
        public static string Hedefler {
            get {
                return ResourceManager.GetString("Hedefler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hepsi.
        /// </summary>
        public static string Hepsi {
            get {
                return ResourceManager.GetString("Hepsi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string İletisim {
            get {
                return ResourceManager.GetString("İletisim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim Bilgileri.
        /// </summary>
        public static string IletisimBilgileri {
            get {
                return ResourceManager.GetString("IletisimBilgileri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kategoriler.
        /// </summary>
        public static string Kategoriler {
            get {
                return ResourceManager.GetString("Kategoriler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telif Hakkı.
        /// </summary>
        public static string Telifhakkı {
            get {
                return ResourceManager.GetString("Telifhakkı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Filmler.
        /// </summary>
        public static string YeniFilmler {
            get {
                return ResourceManager.GetString("YeniFilmler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En popüler Filmler.
        /// </summary>
        public static string YüksekKaliteFilm {
            get {
                return ResourceManager.GetString("YüksekKaliteFilm", resourceCulture);
            }
        }
		
		/// <summary>
        ///   Looks up a localized string similar to banner.
        /// </summary>
		public static string banner {
            get {
                return ResourceManager.GetString("banner", resourceCulture);
            }
        }
		
		/// <summary>
        ///   Looks up a localized string similar to bannerr.
        /// </summary>
		public static string bannerr {
            get {
                return ResourceManager.GetString("bannerr", resourceCulture);
            }
        }
		/// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
		public static string epostaHaber {
            get {
                return ResourceManager.GetString("epostaHaber", resourceCulture);
            }
        }
		
		/// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
		public static string hedeficerik {
            get {
                return ResourceManager.GetString("hedeficerik", resourceCulture);
            }
        }
		/// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
		public static string begenilenFilmler {
            get {
                return ResourceManager.GetString("begenilenFilmler", resourceCulture);
            }
        }
		/// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
		public static string d3filmler {
            get {
                return ResourceManager.GetString("d3filmler", resourceCulture);
            }
        }
		/// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
		public static string odulFilm {
            get {
                return ResourceManager.GetString("odulFilm", resourceCulture);
            }
        }
		/// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
		public static string bizeUlasin {
            get {
                return ResourceManager.GetString("bizeUlasin", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string istekveOneri {
            get {
                return ResourceManager.GetString("istekveOneri", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string mesajYaz {
            get {
                return ResourceManager.GetString("mesajYaz", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string adiniz {
            get {
                return ResourceManager.GetString("adiniz", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string soyadiniz {
            get {
                return ResourceManager.GetString("soyadiniz", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string mesaj {
            get {
                return ResourceManager.GetString("mesaj", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string gonder {
            get {
                return ResourceManager.GetString("gonder", resourceCulture);
            }
        }
		 /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string hakkimizda2 {
            get {
                return ResourceManager.GetString("hakkimizda2", resourceCulture);
            }
        }
		 /// <summary>
        ///   Looks up a localized string similar to epostaHaber.
        /// </summary>
        public static string hakIcereik {
            get {
                return ResourceManager.GetString("hakIcereik", resourceCulture);
            }
        }
		
		
    }
}

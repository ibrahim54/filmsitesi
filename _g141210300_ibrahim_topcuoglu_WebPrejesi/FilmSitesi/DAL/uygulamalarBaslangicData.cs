﻿using FilmSitesi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSitesi.DAL
{//Oluşturulan tablolara test işlemleri için veri girişi yapılıyor.
    public class uygulamalarBaslangicData : System.Data.Entity.DropCreateDatabaseIfModelChanges<uygulamalarContext>
    {
        protected override void Seed(uygulamalarContext context)
        {
            var Kategori = new List<tbl_Kategori>
                        {
            new tbl_Kategori {KategoriAdi="Aksiyon"},
             new tbl_Kategori {KategoriAdi="Dram"},
              new tbl_Kategori {KategoriAdi="Gerilim"},
               new tbl_Kategori {KategoriAdi="Komedi"},
                new tbl_Kategori {KategoriAdi="Korku"},
                   new tbl_Kategori {KategoriAdi="Macera"}
            };
            context.Kategoriler.AddRange(Kategori);
            context.SaveChanges();

            var Admin = new List<tbl_Admin>
                        {
            new tbl_Admin {AdminAdi="ibrahim",AdminSifre="12345"},
              new tbl_Admin {AdminAdi="admin",AdminSifre="admin"}

            };
            context.Adminler.AddRange(Admin);
            context.SaveChanges();

            var Film = new List<tbl_Film>
            {
            new tbl_Film {FilmAdi ="ESARETİN BEDELİ",tbl_KategoriID=2,resimUrl="/assets/img/film/ESARETİN BEDELİ4259.JPG",YonetmenAdi="Frank Darabont",FilmBilgi="Genç ve başarılı bir bankacı olan Andy Dufresne, karısını ve onun sevgilisini öldürmek suçundan ömür boyu hapse mahkum edilir ve Shawshank hapishanesine gönderilir. Burada başta Red olmak üzere yeni arkadaşlar edinir. Hapishane yaşamını uyum sağlamaya çalışırken diğer yandan da bilgisi ve kültürüyle etrafındaki insanları etkilemeyi başaracaktır. "},
            new tbl_Film {FilmAdi ="ALİ BABA VE 7 CÜCELER",tbl_KategoriID=4,resimUrl="/assets/img/film/ALİ BABA VE 7 CÜCELER7938.JPG",YonetmenAdi="Cem Yılmaz",FilmBilgi="Bahçe cüceleri yapıp satan Şenay Cüccaciye'nin sahibi olan Ali Şenay (Cem Yılmaz) ve İlber (Çetin Altay) işlerini büyütüp dünyaya açılmak için Sofya'da bir fuara katılırlar. Ancak fuarda işler istedikleri gibi gitmez ve başlarını büyük belalara sokarlar. Mançov adlı kötü kalpli bir adamın planlarından haberdar olan Şenay ve İlber, onun hain planlarını durdurabilmek için yeni tanıştıkları Memedov ve Veronika ile büyük bir mücadeleye girişirler. Cem Yılmaz'ın yazıp yöneterek başrolünde oynadığı 4. film olma özelliğini taşıyan Ali Baba ve Yedi Cüceler, Bulgaristan'da çekildi. Filmde Cem Yılmaz ve zengin oyuncu kadrosunun yanında Rus model İrina İvkina da ilk kez bir sinema filminde yer alacak. "},
            new tbl_Film {FilmAdi ="DÖVÜŞ KULÜBÜ",tbl_KategoriID=2,resimUrl="/assets/img/film/DÖVÜŞ KULÜBÜ8301.JPG",YonetmenAdi=" David Fincher,  Chon Kye-Young" ,FilmBilgi="Oregon Üniversitesinde yüksek lisansını yapan Chuck Palanhiuk'un uzak olmayan bir gelecekte geçen ve kafası karışık genç bir erkeği konu alan romanından yola çıkılarak çekilen Fight Club'da filmi anlatan, ünlü bir otomobil firmasında iyi bir işe sahiptir. Tek düze yaşamı kronik uykusuzluk sorunuyla çekilmez bir hale gelmiştir. Ailesi ve yakın bir arkadaşı olmayan Jack doktorunun tavsiyesi üzerine kanserli hastaların terapi grubuna katılır. Bu toplantılar esnasında Marla'yla tanışır o da genç adam gibi hasta olmadığı halde grubun toplantılarına katılmaktadır. Jack'in ve Marla'nın çabaları tüketici kültürünün anlamsızlığına karşı bir duruştur adeta kariyer sahibi ama yanlız insanların bir tepkisi. Jack'ın jenerasyonu ölü bir jenerasyondur. Bir yolculuk sonrası evinin yanmış olduğunu gördüğünde arayabileceği tek kişinin yolculuk sırasında tanıştığı sabun satıcısı Tyler Durden olmasıda adeta bunun bir kanıtıdır. İçilen birkaç biranın ardından park yerinde Tyler, kahramanımızı kendine vurması için kışırtacaktır. Aralarında başlayan bu kavga Jack'in hayatını değiştirecektir. Bir süre sonra Jack Tyler'ın yanına taşınır. Tyler'ın liderliğinde bir dövüş kulübünün kuruluşuyla bu kulübde sayıları elliyi aşmamak kaydıyla genç erkekler birbirleriyle dövüşmeye başlayacaklardır. Kısa sürede popüler hale gelen kulüp ve Tyler Durden hızlı bir şekilde bu ölü jenerasyonun mesihi haline gelir. "},
            new tbl_Film {FilmAdi ="İYİ, KÖTÜ VE ÇİRKİN",tbl_KategoriID=3,resimUrl="/assets/img/film/İYİ, KÖTÜ VE ÇİRKİN7148.JPG",YonetmenAdi="Sergio Leone" ,FilmBilgi="Tuco (çirkin), üzerine ödül konulmuş bir kanun kaçağıdır. Keskin nişancı Blondie (iyi) adlı kovboyla işbirliği yaparak kasabaları dolaşmaktadırlar. Tuco'yu kanun adamlarına teslim eden Blondie, ödülü alıp Tuco'yu asılmaktan son anda kurtarmaktadır. Bir kasabada işlerin ters gitmesi üzerine ortaklıkları bozulur. Melekgöz (kötü) lakaplı Sentenza ise Bill Carson adında büyük miktarda altını ele geçirmiş eski bir askerin izini sürmektedir. Tuco'nun çölde Blondie'yi öldürmek üzere olduğu bir anda Bill Carson'la karşılaşmaları tüm planları değiştirir. Carson, altınları İç Savaş'ın hareketli olduğu bir cephede mezarlığa saklamıştır. Ancak Tuco mezarlığın yerini, Blondie ise mezarın adını öğrenebilmiştir. Mecburen işbirliğine tekrar dönen ikili altınları aramaya koyulur. Sonunda üçünün yolu altınların olduğu yerde birleşir. "},
            new tbl_Film {FilmAdi ="KARA ŞÖVALYE",tbl_KategoriID=1,resimUrl="/assets/img/film/KARA ŞÖVALYE5948.JPG",YonetmenAdi="Christopher Nolan",FilmBilgi="The Dark Knight'da, Batman suça karşı savaşını daha da ileriye götürüyor: Teğmen Jim Gordon ve Bölge Savcısı Harvey Dent’in yardımlarıyla, Batman, şehir sokaklarını sarmış olan suç örgütlerinden geriye kalanları temizlemeye girişir. Bu ortaklığın etkili olduğu açıktır, ama ekip kısa süre sonra kendilerini, Joker olarak bilinen ve Gotham şehri sakinlerini daha önce de dehşete boğmuş olan suç dehasının yarattığı karmaşanın ortasında bulurlar. "},
            new tbl_Film {FilmAdi ="KURALSIZ",tbl_KategoriID=3,resimUrl="/assets/img/film/KURALSIZ6817.JPG",YonetmenAdi="Robert Schwentke" ,FilmBilgi="Tris Prior (Shailene Woodley) sevdiği insanları ve kendini kurtarmak için amansız bir savaş vermektedir. Bir yandan da fedakarlık, kimlik, bağlılık, kurallar ve aşkla ilgili evrensel sorunlarla boğuşmaktadır. Üstelik savaş başlamak üzeredir ve herkes bir tarafı seçmek zorundadır, tarafsızlık söz konusu değildir. Telafisi mümkün olmayan hatalar söz konusudur ve Tris'in tek şansı 'Uyumsuz'luğunu kabul edip öyle mücadeleye devam etmesidir. Veronica Roth'un çok satan kitap serisinin Uyumsuz'dan (Divergent) sonraki ikinci kitabı Kuralsız'ın (Insurgent) film uyarlaması, 20 Mart 2015'te Türkiye'deki izleyicilerle buluştu. "},
            new tbl_Film {FilmAdi ="KUZULARIN SESSİZLİĞİ",tbl_KategoriID=3,resimUrl="/assets/img/film/KUZULARIN SESSİZLİĞİ7234.JPG",YonetmenAdi="Jonathan Demme" ,FilmBilgi="Akademiyi başarıyla bitirmiş olan Clarice Starling artık genç bir FBI ajanıdır.Clarice, sapık bir katilin peşindedir.Katilin elinde bulunan bir kadını kurtarmaya çalışmaktadır.Bu katil, kurbanlarının derilerini yüzebilecek kadar psikopat bir sapıktır.Clarice, bu sapığa ulaşma amacıyla, bir başka psikopat olan ünlü Doktor Hannibal Lecter ile yakınlaşmak gerektiği yönünde bir plan yapar.Fakat, Clarice’in Lecter’dan alacağı bilgiler güvenini kazanmasına bağlıdır. Film, 1992 yılında 7 dalda Oscar’a aday oldu ve en iyi film ve en iyi senaryo uyarlaması dalında ödüle layık görüldü.Bu başarılı yapım, Hannibal Lecter serisinin 1.filmidir ve seride dört film daha yer almaktadır.Serinin diğer filmleri; The Silence of The Lambs(1991), Hannibal(2001), Red Dragon(2002), Hannibal Rising(2007) şeklinde sıralanmaktadır. "}

            };

            context.Filmler.AddRange(Film);
            context.SaveChanges();
        }
    }
}